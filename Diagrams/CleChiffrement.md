
```viz
digraph Initial {
  rankdir=LR;
  fixedsize=true;

  subgraph cluster0 {
    border=none
    color=None

    node [color="#E0F8F7",style=filled, width=.8]
    #edge [color=Red]
    D0 [shape=box,color="#B40404",style=bold,label="Clé de chiffrement"];
    D1 [shape=box,color="#B40404",style=bold,label="Fichier de données à chiffrer"];
    D2 [shape=box,color="#B40404",style=bold,label="Fichier de données chiffrées"];
    D0-> "Lecture"
    D1-> "Lecture"
    "Lecture"  -> Chiffrement;
    Chiffrement -> Ecriture;
    Ecriture-> D2;
  }
}
```
