# Programmation pour débutants

## Biblio
https://programmation.developpez.com/tutoriels/cours-complet-initiation-programmation/?page=introduction

## Introduction : des slides avec des images
### Qu'est ce que la programmation
https://zestedesavoir.com/tutoriels/755/le-langage-c-1/1042_les-bases-du-langage-c/4275_introduction-a-la-programmation/

* Qu'est ce que la programmation
  * définition d'un programme : texte qui liste des commandes qui doivent être exécutées par l'ordinateur  
https://fr.wikipedia.org/wiki/Programmation_informatique
  * réalisation d'un traitement automatique de données
* Ecrire un code : avec quoi, comment ?
* Le faire exécuter avec quoi comment ?
  * avec compilateur
  * sans complilateur

### Qu'est ce qu'un langage, A quoi sert un langage ?
#### Intro
Une question doit certainement vous venir à l’esprit : comment communiquer avec notre processeur sans avoir à apprendre sa langue ?

L’idéal serait de parler à notre processeur en français, en anglais, etc, mais disons-le clairement : notre technologie n’est pas suffisamment évoluée et nous avons dû trouver autre chose. La solution retenue a été de créer des langages de programmation plus évolués que le langage machine, plus faciles à apprendre et de fournir le traducteur qui va avec. Il s’agit de langages assez simplifiés, souvent proches des langages naturels et dans lesquels on peut écrire nos programmes beaucoup plus simplement qu’en utilisant le langage machine. Grâce à eux, il est possible d’écrire nos programmes sous forme de texte, sans avoir à se débrouiller avec des suites de zéros et de uns totalement incompréhensibles. Il existe de nombreux langages de programmation et l’un d’entre eux est le C.

Reste que notre processeur ne comprend pas ces langages évolués et n’en connaît qu’un seul : le sien. Aussi, pour utiliser un langage de programmation, il faut disposer d’un traducteur qui fera le lien entre celui-ci et le langage machine du processeur. Ainsi, il ne vous est plus nécessaire de connaître la langue de votre processeur. En informatique, ce traducteur est appelé un compilateur.

Exemple de programmation et langage machine

* Définir un algorithme
Algorithm
An algorithm is a list of
instructions, procedures,
or formulas used to solve a
problem.
The word derives from the
name of the
mathematician,
Mohammed ibn-Musa al-
Khwarizmi (El-Harezmî),
(780 – 850).

* Programmer un algorithme : les différentes étapes

La réalisation d'un programme se décompose en effet en plusieurs phases. De manière simplifiée, on présente ainsi la genèse d'un programme :
  *   établissement d'un cahier des charges : le problème posé ;
  *   analyse du problème et décomposition de celui-ci en sous-problèmes plus ou moins indépendants et plus simples à résoudre ;
  *  choix de structures de données pour représenter les objets du problème ;
  *   mise en œuvre des différents algorithmes à utiliser pour résoudre les sous-problèmes ;
  *   codage des algorithmes et création du programme dans un langage de programmation donné ;
  *   tests (et corrections) et validation auprès des utilisateurs.


#### Historique des langages + domaines d'usage (web, sciences)
https://en.wikipedia.org/wiki/List_of_programming_languages
Ada Lovelace


### Les composantes d'un ordi

#### Historique des ordis
#### Comment ca fonctionne

### Qu'est ce que python

* langage interprété : pas de compilo
https://fr.wikipedia.org/wiki/Interprète_(informatique)
Un interprète se distingue d’un compilateur par le fait qu’il effectue l’analyse et la traduction nécessaires à l'exécution d’un programme donné non pas une fois pour toutes, mais à chaque exécution de ce programme. L’exécution nécessite ainsi de disposer non seulement du programme, mais aussi de l’interprète correspondant.
faire un dessin

* compilateur --> langage machine --> exécution
* interpréteur --> lecture + traduction + exécution (simultané)

Bien que la distinction entre compilateur et interprète soit réelle, leurs définitions se recoupent parfois et il existe des méthodes intermédiaires entre ces deux techniques.

1.1 C'est quoi Python ?

Le langage de programmation Python a été créé en 1989 par Guido van Rossum, aux Pays-Bas. Le nom Python vient d'un hommage à la série télévisée Monty Python's Flying Circus dont G. van Rossum est fan. La première version publique de ce langage a été publiée en 1991.

La dernière version de Python est la version 3. Plus précisément, la version 3.7 a été publiée en juin 2018. La version 2 de Python est désormais obsolète et a cessé d'être maintenue depuis le 1er janvier 2020. Dans la mesure du possible évitez de l'utiliser.

La Python Software Foundation est l'association qui organise le développement de Python et anime la communauté de développeurs et d'utilisateurs.

Ce langage de programmation présente de nombreuses caractéristiques intéressantes :

    Il est multiplateforme. C'est-à-dire qu'il fonctionne sur de nombreux systèmes d'exploitation : Windows, Mac OS X, Linux, Android, iOS, depuis les mini-ordinateurs Raspberry Pi jusqu'aux supercalculateurs.

    Il est gratuit. Vous pouvez l'installer sur autant d'ordinateurs que vous voulez (même sur votre téléphone !).

    C'est un langage de haut niveau. Il demande relativement peu de connaissance sur le fonctionnement d'un ordinateur pour être utilisé.
    C'est un langage interprété. Un script Python n'a pas besoin d'être compilé pour être exécuté, contrairement à des langages comme le C ou le C++.

    Il est orienté objet. C'est-à-dire qu'il est possible de concevoir en Python des entités qui miment celles du monde réel (une cellule, une protéine, un atome, etc.) avec un certain nombre de règles de fonctionnement et d'interactions.

    * bcp de bibliothèques spécialisées qui contiennent des fonctions pré-écrites pour des besoins récurrents


    Il est relativement simple à prendre en main.
    Enfin, il est très utilisé en bioinformatique et plus généralement en analyse de données.




### Description des points principaux que l'on va aborder

* Variables : différents types
* Calculs compliqués qu'on va vouloir automatiser
* Types d'action :
  * calculs que l'on veut répéter un certain nombre de fois ==> boucles
  * tests conditionnels
* Lecture / Ecriture de fichiers
* Production de graphiques basiques
* ...

### Ecrire un programme lisible ... (voir ou on met cette section)

* Quelqu'un qui ne sait pas ce que vous avez fait doit être capable de le lire et de le comprendre
 * Nom des objets qui ont un sens :
 * Commentaires pour expliquer dans le code

## SEANCE 1 : Qu'est ce que la programmation : premier pas

* Alternance de slides et de TPs interactifs

### Constantes et variables
#### Introduction
* Définitions :
  * du point de vue de l'utilisateur
     * constante : valeur égale tout le temps : SecondesMinute=60
     * variable : objet dont la valeur va pouvoir changer au cours du programme : TempsEcoulé= ...
  * du point de vue de l'ordinateur : le nom n'a aucune importance c'est un case en mémoire qui occupe une certaine place

* Opérateurs

#### Différents types de variables
* Le type d’une expression ou d’une variable indique le domaine des valeurs qu’elle peut prendre et les opérations qu’on peut lui appliquer.
Exemples : division d'entier

* Liste des types élémentaires en python

* Opérateurs qui peuvent s'appliquer sur les variables
  * Mathématiques
  * Logiques et relationnels
Exemples

* Affectation d'une variable : comment associe-t-on le nom d'une variable à une opération ?

Exemples

* Affichage au cours du programme des différents types

### TP 0
* Installer python et un éditeur
* Lancer l'interpréteur et faire un calcul en ligne

### TP 1

* Initialiser une variable numérique
* L'afficher
* Faire un calcul basique
* Afficher le résultat
* Idem avec une chaîne de caractère  

Un petit problème concret :

P1) Un marchand de fruits et légumes a une liste de prix : il augmente le prix du kilo de tomates de 10% et celui des pommes de 5%. Faire un programme qui affiche le prix du kilo de tomates et de pommes avant et après augmentation.
P2) Faire afficher un message qui lit le prix initial du kilo de tomates et l'augmentation et qui affiche le nouveau prix en sortie.
P3) Introduire un test dans le programme : si l'augmentation proposée est supérieure à 100% renvoyer un message de refus




## SEANCE 2 : Instructions itératives

* Boucles fixes
* Boucles conditionnelles

## SEANCE 3 : "Sous-programmes", fonctions?...

### Types + complexes : tableaux, chaines, listes, etc ...
