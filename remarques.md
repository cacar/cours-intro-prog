# Définition
La programmation, appelée aussi codage dans le domaine informatique, désigne l'ensemble des activités qui permettent l'écriture des programmes informatiques. C'est une étape importante du développement de logiciels.

# Terminologie
* Informatique : traitement automatique de l’information à l’aide
d’un ordinateur.

* Ordinateur : machine électronique capable d’exécuter un
programme : ordinateur  de bureau, portable, super-calculateur

* Algorithme : suite de prescriptions précises qui indiquent
l’exécution, dans ordre bien déterminé, d’une succession
d’opérations en vue de la résolution d’un problème.

* Programme : ensemble d’ordres ou instructions qui agit sur des
données pour produire des résultats.

* Données : définition

* Programmation, appelée aussi codage dans le domaine informatique, désigne l'ensemble des activités qui permettent l'écriture des programmes informatiques. C'est une étape importante du développement de logiciels

# Dessin de structure d'un ordi général

# Instructions
Types d’instructions :
* Instruction arithmétique : addition, soustraction, multiplication,…
* Instruction de transfert : mouvement de l’information d’un emplacement à
un autre (entre le processeur et la mémoire)
* Instruction d’entrée/sortie : lecture ou écriture (caractère, nombre,…)
* Commande de gestion des périphériques (imprimante, disque,…)
* ...

# Dessin de structure de la mémoire

# Schéma d'exécution d'un programme
Ecriture -> compilation -> édition de liens -> lecture des données -> exécution -> résultats


# Les langages de programmation
* langage machine
* assembleur
* langages évolués

## Choix du langage : suivant vos besoins

a/ La programmation impérative correspond aux programmes traditionnels séquentiels (ou
procéduraux) qui sont des suites d'instructions manipulant des variables.
Cette catégorie comprend notamment le langage FORTRAN, le langage Basic historique et le
langage C.
b/ Les langages orientés objets
Ces langages sont devenus extrêmement populaires. Ce sont largement les plus utilisés.
Le programme n'est plus vu comme l'exécution purement séquentielle d'une suite d'instruction se
traduisant assez rapidement en langage proche de la machine mais comme la manipulation d'objets
évolués. Ces objets se rapportent à des entités identifiables du domaine considéré et ils associent
des données avec les traitements sur ces données sous forme de services. Les objets reçoivent aussi
(et répondent à) des événements, notamment dans la mise en œuvre d'interfaces graphiques.

C++
Java
Python
Php

Il y en a des compilés et des non-compilés
c/ les langages fonctionnels
Il sont basés sur la manipulation d'expressions mathématiques uniquement. Il sont mis en œuvre par
emboîtement d'appels de fonctions.
ui considère le calcul en tant qu'évaluation de fonctions mathématiques. Lisp (vient du lambda-calcul)

# Algorithmique
## définition
L'algorithmique est l'étude et la production de règles et techniques qui sont impliquées dans la définition et la conception d'algorithmes, c'est-à-dire de processus systématiques de résolution d'un problème permettant de décrire précisément des étapes pour résoudre un problème algorithmique.

## autre définition
Dans le domaine des mathématiques, dont le terme est originaire, un algorithme peut être considéré comme un ensemble d’opérations ordonné et fini devant être suivi dans l’ordre pour résoudre un problème. En guise d’exemple très simple, prenons une recette de cuisine. Dans chaque recette, une procédure spécifique doit être suivie dans l’ordre. Les différentes étapes de la recette représentent les opérations qui constituent l’algorithme.

Là où les choses se compliquent un peu, c’est que le but d’un algorithme est de résoudre un problème. Il doit donc produire un résultat. Pour créer un algorithme qui puisse fonctionner dans le monde réel, il faut inclure des instructions. Ces dernières lui permettent de s’adapter aux différentes situations auxquelles il peut être confronté. Voilà pourquoi les algorithmes s’apparentent à de gigantesques « arbres » d’instructions.

Comment fonctionne un algorithme informatique ?

Pour effectuer une tâche, quelle qu’elle soit, un ordinateur a besoin d’un programme informatique. Or, pour fonctionner, un programme informatique doit indiquer à l’ordinateur ce qu’il doit faire avec précision, étape par étape.

L’ordinateur  » exécute  » ensuite le programme, en suivant chaque étape de façon mécanique pour atteindre l’objectif. Or, il faut aussi dire à l’ordinateur  » comment  » faire ce qu’il doit faire. C’est le rôle de l’algorithme informatique.

Les algorithmes informatiques fonctionnent par le biais d’entrées (input) et de sortie (output). Ils reçoivent l’input, et appliquent chaque étape de l’algorithme à cette information pour générer un output.

**Par exemple, un moteur de recherche est un algorithme recevant une requête de recherche en guise d’input. Il mène une recherche dans sa base de données pour des éléments correspondant aux mots de la requête, et produit ensuite les résultats.**

Un autre exemple concerne la Timeline de Facebook. En effet, le contenu que Facebook affiche sur le fil d’actualité des utilisateurs est choisi par un ensemble d’algorithmes. Ces algorithmes décident du contenu à afficher en fonction de divers paramètres. Par exemple, les goûts personnels de l’utilisateur, ses réactions à des contenus précédemment publiés et bien d’autres encore.


Séquence

La séquence constitue le type le plus simple pour enchaîner les instructions qui composent une fonction. Cette forme se distingue par des séries d’étapes, qui seront exécutées les unes après les autres

Boucle

Il arrive qu’il faut exécuter les mêmes commandes à un nombre de reprises variable, et ce, en fonction des entrées de l’algorithme. Pour cette catégorie, le processus s’exécute à répétition en respectant des conditions précises. Elle se traduit par des problèmes sous forme de « oui » et de « non ». Il faut cependant veiller à ce que le processus aboutisse après avoir effectué le nombre de boucles requis par la condition. Cette forme d’algorithme est aussi appelée « type de répétition ».


Branchement

Cette forme est illustrée par la condition « Si ». Si la condition de départ est correcte, la sortie correspondra à A. Dans le cas contraire, le résultat sera B. Ce genre d’algorithme s’appelle également « modèle de sélection ».


## Exemples : pseudo-code



#Quelques éléments clés de programmation
On s'intéresse ici à la programmation impérative séquentielle.
Un programme utilise des constantes, des mots réservés, des variables.
## Une constante est une valeur fixe, représentée par un symbole et utilisable partout dans le
programme. Il faut utiliser les constantes autant que possible car leur définition peut être centralisée
à un même endroit. Si au contraire on utilise des valeurs « en dur » directement dans le programme,
on aura du mal à les retrouver ultérieurement s'il faut les modifier et il y a un risque d'erreur
important.
## Un mot réservé est un élément réservé du langage qui a une signification est une fonction
précise et qui ne peut pas être utilisé autrement. Par exemple il ne doit pas servir comme nom de
constante ou de variable.
## Les variables
Ce sont les éléments essentiels d'un programme. C'est par l'intermédiaire des variables qu'un
programme manipule les données, qu'elles soient sous forme numériques ou textuelles.
Une variables est représentée par un symbole (c'est le nom de la variable) et contient une valeur,
simple ou complexe. Une valeur simple peut-être un caractère unique, un entier, un nombre réel ou
encore une valeur logique (valeur booléenne qui n'a que deux états possibles, vrai ou faux). Une
valeur complexe est composée de plusieurs éléments simples et elle peut-être une chaîne de
caractères, un tableau (d'entier, de réels), une structure (c'est un composé de divers éléments simples
de type différents) .
En fait, une variable représente un espace de stockage en mémoire vive de l'ordinateur. Les
variables permettent donc d'accéder à la mémoire de l'ordinateur pour y lire, stocker et manipuler
des informations. Grâce aux variables, on accède à la mémoire par l'intermédiaire de symboles
auxquels le programmeur attribue une signification précise, et cela dispense de devoir manipuler
directement des adresses mémoire comme en langage assembleur.
