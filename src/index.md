---
marp: true
theme: gricad
author: C. Acary-Robet ; P-A Bouttier
paginate: true
footer: "Introduction à la programmation - CED2023"
---

# Introduction à la programmation

## Cours CED 2024-2025

[Céline Acary-Robert](mailto:celine.acary-robert@univ-grenoble-alpes.fr), [Pierre-Antoine Bouttier](mailto:pierre-antoine.bouttier@univ-grenoble-alpes.fr)

---
## Plan
- [Introduction](https://cacar.gricad-pages.univ-grenoble-alpes.fr/cours-intro-prog/Introduction.html) - [**PDF**](https://cacar.gricad-pages.univ-grenoble-alpes.fr/cours-intro-prog/Introduction.pdf)
- [Partie I : Les systèmes informatiques](https://cacar.gricad-pages.univ-grenoble-alpes.fr/cours-intro-prog/P1_Introduction_contexte.html) - [**PDF**](https://cacar.gricad-pages.univ-grenoble-alpes.fr/cours-intro-prog/P1_Introduction_contexte.pdf)
- [Partie II : Concepts fondamentaux](https://cacar.gricad-pages.univ-grenoble-alpes.fr/cours-intro-prog/P2_Concepts_fondamentaux.html) - [**PDF**](https://cacar.gricad-pages.univ-grenoble-alpes.fr/cours-intro-prog/P2_Concepts_fondamentaux.pdf)
- [Partie III : Outils et bonnes pratiques](https://cacar.gricad-pages.univ-grenoble-alpes.fr/cours-intro-prog/P3_Outils_BonnesPratiques.html) - [**PDF**](https://cacar.gricad-pages.univ-grenoble-alpes.fr/cours-intro-prog/P3_Outils_BonnesPratiques.pdf)
- [Partie IV : Introduction au langage python](https://cacar.gricad-pages.univ-grenoble-alpes.fr/cours-intro-prog/P4_Introduction_python.html) - [**PDF**](https://cacar.gricad-pages.univ-grenoble-alpes.fr/cours-intro-prog/P4_Introduction_python.pdf)
