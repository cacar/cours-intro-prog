---
marp: true
theme: gricad
author: C. Acary-Robet ; P-A Bouttier
paginate: true
footer: "Introduction à la programmation - CED2022"

---
# Concepts fondamentaux de la programmation

 + La **programmation** : ensemble des activités qui permettent l'écriture des programmes informatiques.
   + Conception du problème
   + Ecriture d'un algorithme
   + Traduction dans un langage de programmation choisi

On parle aussi de **développement logiciel**.

---
# Plan de cette partie

+ Définition d'un algorithme
+ Définition d'un programme informatique
+ Elements constitutifs d'un programme
  + Variables
  + Opérations faites sur ces variables

---
## Algorithmie : définition

+  Un algorithme considère des **données en entrée**, exprime un **traitement** particulier et explicite des **données en sortie**.

+   Un algorithme constitue un élément abstrait permettant de définir une méthode pour arriver à un résultat. Par exemple, des formules mathématiques pour faire un calcul, ou une recette de cuisine.  

---
## Algorithme : les étapes

* Identifier les variables à traiter 
  * les données d'entrée
  * les variables intermédiaires de calcul
  * les résultats de sortie
  
* Identifier les étapes à réaliser
  * lecture de données (fichiers, saisie, ...)
  * calculs à effectuer 
  
* Traitement des sorties 
  * écriture des résultats dans un fichier, etc ...
  
  

---
## Algorithmie : exemple

Algorithme qui permet de calculer le volume d'un cylindre : Vol = π x r² x Hauteur

**Données d'entrée** : 
+ le nombre pi
+ les caractéristiques du cylindre (hauteur et diamètre du cercle de base)

**Instructions** :
+ calcul de r^2
+ multiplication par la hauteur H
+ multiplication du résultat précédent par pi 

**Résultat**
+ volume du cylindre

---
# Différence entre algorithme et programme

   Un programme informatique est une traduction de l'algorithme dans un langage de programmation qui cherche à *humaniser les langages cryptés des machines*.

*Remarque : on n'écrit pas les programmes en langage machine directement : illisible et impossible à corriger !*

![w:500 center](fig/binaire.jpeg)

<!-- <img src="./fig/binaire.jpeg" width="500" /> -->

---
# Propriétés d'un algorithme

Un algorithme ne dépend pas :
  + du langage dans lequel il est implanté,
  + de la machine qui exécutera le programme correspondant

---
## Exemple : un algorithme de chiffrement de données

![w:700 center](fig/AlgoChiffrement.png)

<!-- <img src="./fig/fig_algo_chiffrement.png" width="1000" /> -->

---
## Ecriture d'un algorithme 

+ Permet d'expliciter le traitement des variables et les différentes étapes
+ Dans un langage universel : pseudo-code

```
Programme Carré : calcule le carré du nombre donné par l'utilisateur 

variables : nombre, carré_du_nombre

début

   afficher('quel nombre voulez-vous traiter ?')
   saisir(nombre)
   
   carré_du_nombre = nombre * nombre
   
   afficher('Le carré de", nombre, "est :", carré_du_nombre)

fin
```

---
## Programmation

**Méthodologie**

+ Formulation générale d'un problème
+ Ecriture d'un algorithme
+ Choix d'un langage approprié
+ Ecriture du programme

---
## Spécification d'un problème
Tout problème peut se décrire par un ensemble d'éléments de ces quatre types :

* **Paramètre d'entrée**
  + élément variable de l'énoncé du problème.

* **Pré-condition**
  +  une condition que doivent vérifier les paramètres d'entrée --> en dehors de cette condition, le problème n'a pas de sens.

* **Paramètre de sortie**
  + un élément de la réponse au problème.

* **Post-condition**
  + une condition que doivent vérifier les paramètres de sortie --> en dehors de cette condition, la solution apportée au problème n'est pas correcte.

---
## Formulation générale d'un problème

### Exemple

    Maia a n bonbons. Elle en donne p à Léonie. Calculer combien il reste de bonbons à Maia (b).

Il va falloir spécifier des conditions pour que le programme final renvoie des valeurs acceptables.

*Données entrées* : n,p : entiers positifs
*Données de sortie* : b : entier positif ou nul
*Condition d'entrée* : n >= p
*Remarque* : si le programme est correct b > 0

---
## Exemple

**Si on va un peu plus loin** :

Maia a n bonbons. Elle en donne p à Léonie et q à Charles. Calculer combien il reste de bonbons à Maia b.

*Condition d'entrée* : n > (p+q)

---
## Définition d'un paradigme de programmation

Un paradigme de programmation est une façon (parmi d'autres) de concevoir un problème et sa formalisation dans un langage de programmation approprié.

Un paradigme de programmation conditionne aussi la façon dont le programme va s'exécuter.

+ **Programmation orientée objet** : le programme est vu comme une collection d’objets en interaction
+ **Programmation fonctionnelle** : un programme est vu comme une suite d'évaluations de fonctions. 
+ **Programmation parallèle** : le programme est découpé en différentes tâches exécutées simultanément
+ **Programmation impérative** : décrit les opérations en séquences d'instructions devant être exécutées par l'ordinateur (la plus répandue)
+ ...

---
## TP 0 : mise en place de l'environnement de programmation

+ Installation de python
Suivre les instructions ici : [python.org](https://www.python.org/downloads/)
   - sous linux apt-get install python3
   - pour Windows
     1. Téléchargez le programme d'installation du fichier exécutable Windows x86-64 de Python 3.11
     2. Exécutez le programme d'installation.
     3. Choisissez Add Python 3.11 to PATH (Ajouter Python 3.11 à PATH).
     4. Choisissez Install Now (Installer maintenant).


---
## TP 0 : mise en place de l'environnement de programmation


+ Installation de VScode
Suivre les instructions ici : [VSCcode](https://code.visualstudio.com/download)
en fonction de votre OS.

---
## Notions de variables

  + Définition
  + Différents types de variables

Exemples suivants écrits dans un pseudo-langage car tout ce qu'on va aborder dans cette partie ne dépend pas du langage. 


---
## Variables : définition générale

**Les variables sont des éléments qui associent un nom (l'identifiant) à une valeur et à une adresse**.

![w:700 h:300 center](fig/CaseMemoire.png)


---
## Variables : définition générale
### Traitement par l'ordinateur

+ la case mémoire d'un ordinateur ~ un octet = 8 bits
+ 1 bit prend 2 valeurs 0 ou 1 (mode binaire)
+ Exemple : écrire 145 en décimal c'est EXACTEMENT la même chose que d'écrire 10010001 en binaire !
+ les octets (ou bytes) sont lus à la suite pour former des nombres plus grands

---
## Variables : définition générale
### Nature des variables

+ **La valeur peut être de nature différente : nombre, texte, ...**
+ Exemples :
  + A=4
  + prenom1="toto"
  + B= {1,2,3,4}

---
## Variables : définition générale

Les valeurs associées à ces variables peuvent varier au cours de l'exécution du programme.
```
Déclaration : prenom1, prenom2

Première lecture à l'écran d'un prénom : Axel
prenom1="Axel"

Deuxième lecture à l'écran d'un prénom : Jean
prenom1="Jean"

Troisième lecture à l'écran d'un prénom : Anna
prenom2="Anna"

```

---
# Manipulation des variables

Manipulations usuelles :

  + Initialisation au début d'un programme : déclaration
  + Changement de valeur au cours du programme : affectation
  + Lecture/écriture dans un fichiers
  + Lecture à l'écran

La mise en oeuvre concrète de ces règles dépendent du langage.

---
# Variables : définition complète

+ **Nom de la variable** = nom dans le programme + **étiquette** (une référence, un pointeur) qui indique l'adresse en mémoire
+ **Valeur** : valeur de la données qui est stockée à un instant donné


---
# Variables : utilisation

+ Définition des variables dont on a besoin
+ Opérations (parfois complexes) sur ces variables


---
# Manipulation des variables
  **Schéma de lecture de données**

![w:700 h:500 center](fig/LectureStockage3.png)


---
## Utililisation des variables : exemple
On veut construire un algorithme qui lise 2 valeurs A et B, qui échange les valeurs de A et de B et qui affiche les nouvelles valeurs. 
```
programme echange_de_valeurs 

variables : A, B 

début
  saisir(A)
  saisir(B)
  
  A = B 
  B = A
  
  afficher('les nouvelles valeurs sont :')
  afficher(A,B)
  
fin
```

Que fait ce programme ? 


---
## Utililisation des variables : exemple (suite)

Solution : on a besoin d'une variable pour stocker temporairement une valeur 


```
programme echange_de_valeurs 

variables : A, B, C 

début
  saisir(A)
  saisir(B)
  
  C = A 
  A = B
  B = C
  
  afficher('les nouvelles valeurs sont :')
  afficher(A,B)
  
fin
```

---
# Type d'une variable

Le type d’une variable détermine :

  + l’ensemble de valeurs possibles (entiers, flottants, etc.),
  + la quantité de mémoire à réserver (sizeof),
  + le codage utilisé en mémoire (texte),
  + la sémantique des opérations (division /)(exemple : entiers).

*Remarque : le nom du type d'une variable va dépendre du langage*

---
# Liste des types principaux de base

+ entiers,
+ flottants,
+ booléens (true ou false)
+ chaîne de caractère
+ "tableaux" des précédents types
+ ...
**Chaque type de variable prend plus ou moins d'espace en mémoire.**

---
# Chaînes de caractères : encodage

Pour votre ordinateur, une lettre c'est un nombre !
En effet, il existe des encodages qui permettent d'associer un numéro à chaque lettre.

+ Le premier codage est l'**ASCII** (American Standard Code for Information Interchange, 1960). Ce codage était très incomplet : caractères de base (alphabet de A à Z et de a à z), nombre de 0 à 9 et les caractères comme les parenthèses, virgules...

+ l'**ISO_8859-1** a été mis au point. Ce dernier reprend le codage ASCII en ajoutant des caractères spéciaux (é, à, è, î, ï, ù...).

+ l'**UTF-8** qui rajoute encore d'autres caractères spéciaux

*Remarque : Unicode est une norme qui spécifie un ensemble de couples (caractère, point de code), UTF-8 est une façon d'encoder les caractères définis dans cette norme.*

---
# Eléments d'un programme

Quels sont les différents types d'éléments que l'on va rencontrer dans un programme ? Ce sont des outils qui vont permettre de traiter les variables pour obtenir le résultats souhaité. 

**Exemples :**

+ Opérations classiques sur les nombres, traitement statistique
+ Opérations sur les chaînes de caractères : concaténation, extraction, etc ...

---
# Eléments d'un programme

+ Appel à des fonctions fournies par le langage
  + Fonctions de traitement des variables
    + Exemple : type(x) : renvoie le type de la variable x

  + Fonctions mathématiques prédéfinies
    + Exemple : fonction sinus, exponentielle, ...

  + Graphiques, ...

---
# Eléments d'un programme : boucles

**Définition** : une ou plusieurs instructions qui vont être répétées un nombre fixe de fois

**Intérêt** : n'écrire les instructions qu'une seule fois en quelques lignes de code

**Exemple** : algorithme de chiffrement de plusieurs fichiers de données

---
# Utilisation de boucles : exemple (1)

+ Exemple : on veut écrire à l'écran N fois le caractère #

```python
Variables N, i : Entier

Debut du programme

Ecrire "Entrez un nombre : "
Lire N

i ← 0
# On veut écrire à l'écran le caractère *
Pour i=1 à i=N
   Ecrire *
FinPour

Fin du programme
```

---
# Utilisation de boucles : exemple (2)

+ Autre exemple : l'instruction dépend de l'indice de boucle

```python
Variables N, i Entier

Debut du programme
Ecrire "Entrez un nombre : "
Lire N

i ← 0
Ecrire "Les 10 nombres suivants sont : "
Pour i=1 à i=10

   Ecrire N + i

FinPour
Fin du programme
```
---
# Utilisation de boucles : exemple (3)

+ Autre exemple : l'instruction dépend deux fois de l'indice

On veut ajouter à chaque case d'un tableau d'entier l'indice de la case : 

```python
Pour i=0 --> i=10
   tab[i] = tab[i] + i
FinPour
```

---
## Eléments d'un programme : les structures conditionnelles

Les instructions conditionnelles permettent d'exécuter des instructions suivant le test d'une variable

```python
N=10
Si N < 10 alors
   Afficher N
FinSi
```

Exemples de tests
+ i = 10
+ i < 10, i > 10
+ Valeur = vraie (booléens)


---
# Structures conditionnelles

On peut créer une alternative :

```python
N=10
Si N = 0 alors
   Afficher "on ne peut pas diviser par zero"
Sinon
   Afficher 100/N
FinSi
```

+ On peut ainsi imbriquer plusieurs tests et sous-tests

---
# Structures conditionnelles

Tester plusieurs valeurs à la fois :

```python
N1=10
N2=3
Si N1 = 0 ou N2 = 0 alors
   Afficher "on ne peut pas diviser par zero"
Sinon
   Afficher 100/N1 et 100/N2
FinSi
```


---
## Eléments d'un programme : do while

Permet de combiner une instruction de boucle et un test

```python
N = 0
Tant que N < 100 alors
   Afficher "N est inférieur à 100"
   N = N + 1
FinTantQue
```
+ Si on oublie d'incrémenter N : on a souvent une boucle infinie !


---
## Do while : exemple d'utilisation

+ Lire un nombre et faire afficher les 10 suivants : une première solution

```python
Variables N, Stop en Entier

Debut

Ecrire "Entrez un nombre : "
Lire N
Stop ← N + 10

Ecrire "Les 10 nombres suivants sont : "
TantQue N < Stop
   N ← N+1
   Ecrire N
FinTantQue

Fin
```
---
## Do while : exemple d'utilisation

+ Lire un nombre et faire afficher les 10 suivants : une autre solution

```python
Variables N, i en Entier
 
Debut

Ecrire "Entrez un nombre : "
Lire N
i ← 0

Ecrire "Les 10 nombres suivants sont : "
TantQue i < 10
   i ← i + 1
   Ecrire N + i
FinTantQue

Fin
```




---
#  Eléments d'un programme : Entrées - sorties

+ **Entrées** : données qui sont fournies à l'algorithme avant exécution
  + Valeur saisie à l'écran
  + Données lues dans un fichier
  + Sorties d'un programme

+ **Sorties** : données qui sont produites par l'algorithme en cours ou à la fin de l'exécution (résultat final ou intermédiaire)
  + Affichage à l'écran
  + Données écrites dans un fichier
  + Transmises à un autre programme



---
# Eléments d'un programme : les fonctions définies par le programmeur

Fonction = bloc d’instructions

+ déclarée une fois, réutilisable de nombreuses fois,
+ peut prendre des arguments et retourner une valeur (une entrée et une sortie),
+ a un effet (modification de la mémoire, affichage à l’écran),
+ peut être définie dans un autre **module**, entité, fichier, etc ...

---
# Les fonctions : exemple

Exemple de fonction

```python
Définition fonction Calcul_de_moyenne
    moyenne = 0.5 * (nombre1 + nombre2)
FinFonction
```

---
# Eléments d'un programme : les fonctions

Les fonctions peuvent avoir des arguments

```python
Définition fonction Calcul_de_moyenne(nombre1, nombre2)
    moyenne = 0.5 * (nombre1 + nombre2)
FinFonction
```


---
# Eléments d'un programme : les fonctions

Les fonctions peuvent retourner une valeur

```python
Définition fonction Calcul_de_moyenne(nombre1, nombre2)
    moyenne1 = 0.5 * (nombre1 + nombre2)
    retourne moyenne1
FinFonction

Programme principal
nombre1 = 10
nombre2 = 33
moyenne = Calcul_de_moyenne(nombre1, nombre2)
```

---
## Dépendances externes (librairies, sous-programmes, ...)

Quelque soit le langage utilisé, il existe des bibliothèques de fonctions déjà écrites, bien optimisées, qu'on doit utiliser en priorité :

+ bibliothèque standard : affichage graphique, etc ...
+ bibliothèque mathématique, etc.
+ Il faut les importer par une directive spéciale qui dépend du langage

---
## Exercice

Ecrire un algorithme qui cherche dans un tableau le nombre 12. La taille du tableau est N. Si on trouve 12 on affiche un message et le programme s'arrête.


---
## Une solution

```python
Variable N,i : Entier

N ← 100
i=0

Debut

TantQue i < N
  Si tableau(i)==12
     afficher "on a trouvé 12 à la case i"
     stop
  Sinon
     i=i+1
  FinSi
FinTantQue

Fin
```

---
## Exercice

Ecrire un algorithme qui demande un nombre de départ, et qui calcule la somme des entiers de 1 jusqu’à ce nombre. Par exemple, si l’utilsateur saisit 5, le programme doit calculer :
 1 + 2 + 3 + 4 + 5 = 15
On souhaite afficher uniquement le résultat, pas la décomposition du calcul.

---
## Une solution

```python
Variables N, i, Som : Entier
Som ← 0

Debut

Ecrire "Entrez un nombre : "
Lire N

Pour i ← 1 à N
  Som ← Som + i
FinPour
Ecrire "La somme est : ", Som

Fin
```
