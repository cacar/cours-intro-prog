---
marp: true
theme: gricad
author: C. Acary-Robet ; P-A Bouttier
paginate: true
footer: "Introduction à la programmation - CED2022"

---
# Introduction et contexte

---
## Objectifs de la formation

+ Comprendre les fondamentaux de la programmation informatique 
+ Comprendre, écrire et exécuter des programmes simples, en langage python pour traiter des données textuelles et/ou numériques

---
## Prérequis

+ Un ordinateur
+ Une connexion internet

---
## Un ordinateur ? 

![w:500 center](fig/pano_ordi.jpg)

---
## Un ordinateur ?


Un système informatique ("ordinateur", smartphone, serveur, etc..) est composé de 2 principaux sous-systèmes : 

* Une **unité centrale** : coeur du système informatique, c'est l'ensemble des composants communs à tout système informatique
* Des périphériques : matériels permettant d'envoyer à ou de recevoir des données de l'unité centrale, *e.g.* écran, clavier, disque dur, clef USB

---
## Les périphériques

Les périphériques permettent de recueillir, de stocker et d'exposer des données numériques, face à l'utilisateur ou à d'autres systèmes informatiques :
* Périphériques d'entrées : clavier, souris, tablette graphique, ...
* Périphériques de stockage (secondaire) : disque dur interne, externe, clef USB...
* Périphériques de sortie : enceintes, écran...

---
## L'unité centrale

L'unité centrale d'un système informatique applique **des séquences d'instructions** sur des **données numériques**. Elle est composée de :

* l'unité centrale de traitement, le CPU, le "micro-processeur" : effectue les opérations arithmétiques et contrôle l'ordre dans lequel elles sont réalisées
  + La "puissance" du CPU se mesure en nombre d'opérations qu'il peut effectuer par seconde. L'unité est le Hertz (Hz)
* Le stockage primaire (RAM et ROM) : mémoire pour stocker les séquences d'instructions, les données sur lesquelles elles vont s'appliquer et les données résultant de ces séquences ; RAM et ROM.
* Des **bus** : adressage, mémoire, contrôle, etc. qui va où, quand, pour faire quoi, etc. 

---
## Représentation schématique

![w:500 center](fig/schema_uc.png)

---
## Représentation des données et des séquences

* Une séquence d'instruction est une suite d'opérations arithmétiques, réalisée en appliquant des règles logiques sur un **état binaire (0 ou 1)**, représenté par la présence ou non du courant électrique
* Toute donnée stockée ou traitée par un système informatique est une suite de bits, i.e. 0 ou 1 
* **L'unité de la représentation des données** est une **suite de 8 bits : un octet**. Une donnée est a minima représentée informatiquement par un octet.  
* La capacité de tout système mémoire (primaire, secondaire) se mesure en nombre d'octets qu'elle peut accueillir

---
## Mais quel intérêt ? 

Nous ne pouvons pas, ou alors au prix de grands effort, raisonner en langage binaire : mémorisation de longues suites monotones, représentation des données absconse, décomposition des calculs très longue, etc. Alors, quel est l'intérêt originel de l'ordinateur ? 

* **Séparer la pensée/le raisonnement du calcul**

---
## Pourquoi utiliser un système informatique ?

* Automatiser des tâches...
* ...fastidieuses et répétitives...
* ...et/ou complexes...
* ...nécessitant un nombre d'opérations et/ou un volumes de données trop important

--- 

# Un petit historique très partiel 

## Ordres de grandeurs et évolutions

---
## L'ENIAC, le premier ordinateur électronique (1942)

![w:500 center](fig/eniac.jpg)

* 18000 tubes à vide, 30t, 70m2, 150kW
* Puissance d'environ 200 kHz (pour des additions)
* "Mémoire" de 200 chiffres décimaux

---
## L'UNIVAC, 1er (ou 2ème) ordinateur commercial (1956)

![w:500 center](fig/univac.jpeg)

* 35m2, 13t, 125kW
* Puissance de 2,25MHz
* Mémoire : 1000 mots de 12 bits

---
## L'IBM 5150, premier ordinateur personnel (1981)

![w:500 center](fig/ibm_5150.jpg)

* 4,77MHz, entre 16Ko et 256Ko de mémoire primaire

---
## L'iPhone, la miniaturisation couplée à la puissance (2006)

![w:500 center](fig/iphone.jpg)

* Puissance : 412MHz
* Mémoire RAM : 128Mo

---
## La loi empirique de Moore

  Le nombre de transistors des microprocesseurs sur une puce de silicium double tous les deux ans. Et ce qui est vrai pour le CPU, l'est également pour la RAM ou la capacité de stockage.

Aujourd'hui, les ordres de grandeurs : 
* Ordinateurs : 
  + Puissance CPU : 3GHz, plusieurs "coeurs"
  + Mémoire RAM : 16Go
* Smartphone : 
    + 4 coeurs à 1,8GHz pour l'iphone 13
    + Mémoire RAM : 4Go

---
## Et le logiciel dans tout ça ? 

- Depuis 70 ans, des évolutions foisonnantes dans la partie matérielle de l'informatique :
  - conceptuelles,... 
  - ...physiques...
  - ...et industrielles

* Le monde logiciel a suivi la tendance : 
  * Naissance des systèmes d'exploitation
  * Multiplication des langages de programmation
  * Explosion de la complexité des programmes
  * ...

---

# Les programmes informatiques

---
## Qu'est qu'un programme informatique ? 

Un programme informatique est une suite/une séquence d'instructions et d’opérations destinées à être exécutées par un ordinateur.

* Un **programme source** est un **ensemble d'instructions** écrit par un humain dans **un langage de programmation**. Il peut être compilé vers une forme binaire ou directement interprété.
* Un **programme binaire** décrit les instructions à exécuter par une Unité Centrale de Traitement sous forme numérique. Ces instructions définissent un langage machine. 

---
## Distinction programme/logiciel

Un **logiciel** est **un programme informatique OU un ensemble de programmes** assemblés dans un **objectif de service précis**. 

* Par abus de langage, nous utilisons souvent indifférement l'un ou l'autre terme. 
* Exemples : 
  * Navigateur web, Traitement de texte, tableur, lecteur multimédia, jeux video, etc. 
  * Code de calcul, script de traitement de données
  * Systèmes d'exploitation
  * Driver de carte graphique
  * etc.
  
---
## Le système d'exploitation

* Ensemble de programmes qui dirige l'utilisation des ressources d'un système informatique...
* ...par des logiciels applicatifs
* Exemples : Windows, Linux, macOS, iOS, Android

--- 
## Les interfaces utilisateur

Un grand nombre de logiciel nécessite d'acquérir des données de la part de l'utilisateur humain. Deux grandes familles : 
- L'interface graphique
- L'interface textuel, où les interactions se font dans un **terminal**

---
## Qu'est qu'un programme informatique ? 

Un programme informatique est une suite/une séquence d'instructions et d’opérations destinées à être exécutées par un ordinateur.

* Un **programme source** (ou code source) est un **ensemble d'instructions** écrit par un informaticien dans **un langage de programmation**. Il peut être compilé vers une forme binaire ou directement interprété.
* Un **programme binaire** décrit les instructions à exécuter par un microprocesseur sous forme numérique. Ces instructions définissent un langage machine. 
* **Notre but : apprendre à écrire un programme source**

---
## Les langages de programmations

Un langage de programmation est une notation conventionnelle qui vise à produire des logiciels. 
* Il est défini par **une syntaxe et des règles de grammaire**
* Un *traducteur* qui va convertir de langage en **langage machine** (suite d'instructions binaires)
* Son niveau d'abstraction permet de **s'affranchir des spécifités matérielles** du système pour se concentrer sur la logique de l'algorithme (organisation des intructions), de la représentation des données, etc. 


---
## Deux grandes familles de langages

Les deux grandes familles de langages se distinguent par le mécanisme de *traduction* en langage machine : 

* Lorsque la *traduction* est faite en amont de l'exécution des instructions, on parle de **langage compilé**
* Lorsque la *traduction* est faite pendant l'exécution des instructions, à la volée, on parle de **langage interprété**

--- 
## Les langages compilés

Pour un langage compilé (appelés également **langages statiques**) : 
* Une fois le programme source écrit, on va appliquer un logiciel appelé **compilateur** qui va le convertir en langage machine
* Le compilateur produit un fichier **binaire** **exécutable**, c'est à dire une suite d'octets prêt à être exécuté par l'unité centrale
* Exemples de langages compilés : C/C++, Fortran

---
## Les langages interprétés 

Pour un langage interprété :
* Le code source est exécuté à travers un **interpréteur** : sa traduction n'est pas découplée de son exécution
* Au sens strict du langage interprété, l'interpréteur n'analyse pas l'ensemble du code source avant l'exécution.
* Pour exécuter un code source écrit dans un langage interprété, il faut que l'interpréteur soit déjà installé sur le système. 
* Exemple de langage interprété : Python, R, Julia, Perl, Guile, etc

---
## Pros and cons

Pour simplifier, le programme binaire réellement exécuté d'un langage interprété est considéré :
* Avec un plus haut niveau d'abstraction 
  * plus lisible
  * plus de fonctionnalités facilement accessibles
  * plus sujet aux erreurs de programmation (bugs) 
* Moins performant
* **En réalité, les interpréteurs empruntent des méthodes d'analyse de langages statiques pour diminuer ces effets négatifs**

---

# Les données

---
## De quoi parle-t-on ?

Dans le cadre logiciel, une donnée est une représentation d'une information utile à la séquence d'instructions écrite. Sa source peut être : 
* Dans le code source lui-même 
* Stockée dans le système au préalable, dans un fichier
* Renseignée par l'utilisateur au lancement de ou pendant l'exécution du programme

---
## Les types élémentaires de données

Il existe différents types de données : textuelles, numériques, images, vidéo, etc.

* Dans le cadre d'un langage de programmation, leurs représentations sont faites à partir de 2 familles de type élémentaire de données : 
  * Les caractères alpha-numériques
  * Les nombres (entiers, flottants, i.e. décimal)
* Pour ces deux types élémentaires il existe des normes pour leur représentation en octets (leur traduction en 0 et 1)

---
## La représentation des données 

L'ensemble des représentations des données, quelles qu'elles soient, peut être fait à l'aide de structures, combinant ou non ces 2 types : 

* Un texte est une suite de caractères alpha-numériques
* Une image est un tableau de nombres désignant les 3 valeurs que prennent chaque pixel à l'écran
* etc. 

* En réalité, la plupart des langages de programmation intègrent des types de base allant au-delà du caractère alpha-numérique et du nombre entier ou flottant, pour des raisons de praticité.  

---

# Passons aux concepts fondamentaux de la programmation






