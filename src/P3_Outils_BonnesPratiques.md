---
marp: true
theme: gricad
author: C. Acary-Robet ; P-A Bouttier
paginate: true
footer: "Introduction à la programmation - CED2022"

---
# Partie III : Outils et bonnes pratiques

#### Plan

1. Bonnes pratiques de programmation : définition et aperçu
2. Définition de la reproductibilité et son intérêt
3. Présentation des outils utiles



---
# Bonnes pratiques

- Objectifs finaux :
  * **prêter ou diffuser son code** facilement
  * faire des développements **reproductibles**

* [Plaquette du CNRS : Je code : Les bonnes pratiques de développement logiciel](https://hal.science/hal-02083801v1)
* [Chapitre "Bonnes pratiques" (cours en ligne P.-A. Champin)](https://perso.liris.cnrs.fr/pierre-antoine.champin/enseignement/algo/cours/algo/bonnes_pratiques.html)

---
# Bonnes pratiques

Exemples :

- Code bien écrit
  - choix de noms de variables lisibles
  - bien commenter son code pour faciliter sa compréhension

- Exécution : processus facile à mettre en œuvre
  - fichiers de données facilement accessibles
  - ordre d'exécution de scripts facilement identifiable

---
# Méthodologie générale

### Réflexions préliminaires, en amont de l'écriture du code

==> Spécifications : que va faire mon code ?
  + définir l'objectif du code,
  + établir l’enchaînement logique des étapes,
  + lister les contraintes à respecter,
  + préciser les bibliothèques extérieures à utiliser.

---
# Méthodologie générale

### Réflexions préliminaires

+ Mise en oeuvre : comment je vais organiser les étapes ?
  + définir les différents composants (exemple : fonctions),
  + définir le flux de données et les interfaces entre composants,
  + définir la modularité.

*Remarque : on peut utiliser des diagrammes pour faciliter la représentation.*

---
# Méthodologie générale

### Algorigramme

![w:500 center](fig/EX_ALGO-PORTE_AUTO_1.png)

---
# Méthodologie générale

### Mise en oeuvre

+ Codage : réalisation
  + définir une convention pour nommer les variables
  + mettre des commentaires dans le code
  + intégration de codes existants : attention aux licenses
  + choix du langage : facilité de mise en oeuvre, maîtrise, performance

+ Faire une documentation qui explique les développements, les méthodes utilisées, etc ....


---
# Propriétés d'un code bien écrit

+ Facile à lire, **par soi-même et par les autres**
+ Un code brouillon :
  + est très difficile et fatiguant à comprendre,
  + les bugs s'y cachent beaucoup plus facilement,
  + le reprendre et ajouter des fonctionnalités ultérieurement devient impossible.

---
# Propriétés d'un code bien écrit

+  Avoir une organisation logique et évidente.
  + Mise en oeuvre d'une méthode la plus simple possible
  + Exemple :  vous voulez afficher tous les nombres de 1 à 10
    + Solution 1 : faire évoluer un compteur entre 1 et 10 et qui affiche la valeur de ce compteur
    + Solution 2 : faire évoluer un compteur de 9 à 0 et qui affiche le résultat de 10 - compteur

La solution 2 fonctionne aussi mais est généralement à éviter !

---
# Propriétés d'un code bien écrit

+  Être explicite, montrer clairement les intentions du développeur.
   Si on reprend l'exemple de l'affichage du compteur : expliquer clairement pourquoi on choisit une solution qui ne paraît pas évidente à première vue !

---
# Propriétés d'un code bien écrit

+  Être soigné et robuste au temps qui passe : le code doit être entretenu !
  + Pouvoir relire son code longtemps après l'avoir terminé,
  + Vérifier que l'on a bien supprimé les éléments obsolètes,
  + Vérifier que les commentaires sont à jour et cohérents avec le code conservé.

Cette **opération de "maintenance" du code est cruciale**, mais elle est pourtant souvent négligée par beaucoup, ce qui peut poser des problèmes, notamment lorsque vous rencontrez un bug.

---
# Un code bien écrit permet de ...

+ Gagner du temps ....
   +  Si vous adoptez les bonnes pratiques dès le début, vous faites déjà 50% du travail.
   +  Si le code est bien écrit, il est plus facile, et donc plus rapide à relire,
   +  Si le code est logique et bien structuré, il sera plus facile de trouver les bugs qu'il contient, et donc de l'améliorer..


---
# Commentaires

Les commentaires sont essentiels pour "éclairer" le code. Un commentaire est ignoré par l'ordinateur lorsqu'il exécute le programme mais lu par les développeurs !


---
# Choix judicieux des nom des variables

+ Nom de variable explicite
  + a est bien moins explicite que adresseClient
  + Recherche des occurences des variables
    --> À votre avis, combien d'occurrences de a allez vous trouver ? Et combien d'occurrences de adresseClient ?

---
# Choix judicieux des nom des variables

+ Évitez les contre-sens
  + Exemple : `matrice=8`, on pourrait penser que la variable est une matrice mais c'est un entier. Au moment de l'affectation, il est facile de se rendre compte du type de la variable, mais maintenant, imaginez que vous rencontriez, au beau milieu du code, la ligne suivante :
```
matrice = matrice * 4
```
Comment allez vous interpréter cette instruction ?

---
![w:500 center](fig/SeRelire.png)

<!-- <img src="./fig/SeRelire.png" width="500" /> -->

---
# Objectif : faire de la recherche reproductible

Tous les éléments vus précédemment vont être essentiels pour pouvoir faire de la recherche reproductible.



---
# Reproductibilité

**Définition**

Dictionnaire : Faculté d'être reproduit

En science :
Donner la possibilité de reproduire une expérience et obtenir des résultats comparables, ou similaires, mais c’est dans le détail que les objectifs diffèrent.

---
# Reproductibilité

**Définition** de Roberto Di Cosmo, (2020) :

Dans la reproductibilité, ce qui importe c’est pouvoir reproduire une expérience dans des conditions similaires. Ceci pourra être fait avec d’autres données, ou avec des hypothèses sensiblement différentes. Le résultat sera de fait différent, mais comparable, cohérent (consistent en anglais) avec les résultats originaux.

Pour le développement de code : même code + même données = mêmes résultats  

---
# Reproductibilité

**Définition**

![w:500 center](fig/39-reproducible-replicable-robust-generalisable.jpg)
[CC-BY The Turing Way Community and Scriberia, 2020]

---
# Reproductibilité

Etre capable, plusieurs années après la création des programmes informatiques, de pouvoir les exécuter et d'obtenir des résultats similaires c'est à dire d'avoir :

+ Accès au code, savoir/pouvoir l'exécuter
+ Accès au données, pouvoir les lire
+ Accès à l'interpréteur, compilateur, ...

**On peut être obligé de faire évoluer le code au cours du temps pour pouvoir continuer à l'utiliser.**

---
# Reproductibilité

**Gérer ses développements** : le choix de nom des répertoire

![w:500 center](fig/GestionDossiers.jpg)
[Valérie Orozco, 2020]

---
# Reproductibilité

Pour faire des développements reproductibles, il faut :

+ Avoir des sauvegardes bien organisées
+ Savoir quelle est la version du code utilisée (dans une publication par exemple)
+ Avoir une documentation claire de ce qu'il y a dans les différents fichiers du code mais aussi de :
  + la chaîne d'exécution
  + le nom et la localisation du ou des fichier(s) de données
  + mais aussi des librairies externes et leur version


---
# Outils utiles

Plusieurs familles d'outils utiles pour :
* Faciliter l'écriture
* Faciliter la collaboration
* Favoriser la pérennité des développements


---
# Outils utiles : éditeur de code

+ Utiliser un éditeur qui mette en évidence la syntaxe du langage
  + permet d'identifier des erreurs rapidement
  + permet l'exécution directe du programme
  + contient souvent un *debugger*

---
# Outils utiles : gestionnaire de version et travail collaboratif

+ Utiliser un gestionnaire de version
  + permet de conserver un historique de son développement
  + permet de revenir à une version précédente en cas d'erreur
  + permet de développer "facilement" plusieurs versions d'un même code
  + permet le travail collaboratif : intervenir à plusieurs sur un code sans fausse manipulation
  + exemple : git, mercurial, (svn, etc ...)

---
# Gestionnaire de version et travail collaboratif

Travail collaboratif :  **développer à plusieurs sur un même code**
+ Difficultés
  + Avoir la dernière version
  + Ne pas faire des mofications chacun dans son coin de façon indépendante
  + Ne pas écraser les modifications des collègues

---
# Gestionnaire de version et travail collaboratif

L'utilisation d'un gestionnaire de version permet d'éviter toutes ces difficultés
+ Le gestionnaire de version permet de savoir quelles sont les dernières modifications apportées au code, quand et par qui
+ Il permet aussi d'être sûr de récuperer la **toute dernière version**

+ Dropbox n'est pas un gestionnaire de version !!


---
# Outils utiles : gestionnaire de version et travail collaboratif

![w:800 center](fig/Git.jpg)



---
# Notebooks Jupyter : présentation

+ Les notebooks Jupyter sont des cahiers électroniques constitués de différentes cellules.

+ Ils permettent, dans le même document, de rassembler du texte, des images, des formules mathématiques et du code informatique exécutable.

+ Ils sont manipulables interactivement dans un navigateur web.

Initialement développés pour les langages de programmation Julia, Python et R (d'où le nom Jupyter), les notebooks Jupyter supportent près de 40 langages différents.

---
# Notebooks Jupyter :

![w:800 center](fig/Notebook.png)


---
# Notebooks Jupyter : présentation

+ Un notebook peut être exporté sous différents formats (html, pdf, latex, ...)
+ jupyter-notebook peut s'installer sur votre machine et les notebooks s'affichent ensuite dans votre navigateur web.

---
# Notebooks Jupyter : "défauts"

+ Peut-être lent au démarrage
+ Peut-être lent à l'éxecution de code (gestion de cache)
+ Gestion des versions de code un peu compliquée
+ Reproductibilité : possible mais pas immédiate

Conclusion : outil très intéressant pour explorer, tester, expliquer mais pas pour de la production

---
# Notebooks Jupyter : démo

1. Ouvrir dans votre navigateur le lien :
[jupyterlite](https://bouttiep.gricad-pages.univ-grenoble-alpes.fr/jupyterlite/lab/)

2. Dans le menu à gauche, cliquer sur le répertoire notebook

3. Ouvrir le notebook "P3_demo.ipynb"
