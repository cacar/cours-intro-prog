---
marp: true
theme: gricad
author: C. Acary-Robet ; P-A Bouttier
paginate: true
footer: "Introduction à la programmation - CED2022"


---
# Introduction aux spécifités du langage python

+ Spécificités du langage
+ Définition des variables
+ Syntaxe des différents éléments d'un programme

---
# Qu'est ce que python ?

### Création

Le langage de programmation Python a été créé en 1989 par Guido van Rossum, aux Pays-Bas.
Le nom Python vient d'un hommage à la série télévisée Monty Python's Flying Circus dont G. van Rossum est fan.
La première version publique de ce langage a été publiée en 1991.

---
# Qu'est ce que python ?

La **[Python Software Foundation](https://www.python.org/psf-landing/)** est l'association qui organise le développement de Python et anime la communauté de développeurs et d'utilisateurs.


### Evolution

La dernière version de Python est la version 3. Plus précisément, la version 3.7 a été publiée en juin 2018.

**La version 2 de Python est désormais obsolète et a cessé d'être maintenue après le 1er janvier 2020 ==> on n'utilisera pas cette version.**

---
# Caractéristiques générales du langage

 + Il est relativement simple à prendre en main.

 + Il est multiplateforme : il fonctionne sur de nombreux systèmes d'exploitation (Windows, Mac OS X, Linux, Android, iOS, ...)
       depuis les mini-ordinateurs Raspberry Pi jusqu'aux supercalculateurs.
 + Il est gratuit. Vous pouvez l'installer sur autant d'ordinateurs que vous voulez (même sur votre téléphone !).
 + C'est un langage de haut niveau. Il demande relativement peu de connaissance sur le fonctionnement d'un ordinateur pour être utilisé.

---
# Caractéristiques générales du langage

+ C'est un langage interprété. Un script Python n'a pas besoin d'être compilé pour être exécuté, contrairement à des langages comme le C ou le C++. Chaque ligne de code est lue puis interprétée afin d'être exécutée par l'ordinateur.
  + Il est orienté **objet**. C'est-à-dire qu'il est possible de concevoir en Python des entités qui miment celles du monde réel (une cellule, une protéine, un atome, etc.) avec un certain nombre de règles de fonctionnement et d'interactions.
  + Il peut très bien être utilisé en pur impératif (suites d'instrcutions sans l'abstraction objet)

---
# Bibliographie

+ Documentation officielle sur le site [Python Software Foundation](https://www.python.org/psf-landing/)
+ Sites web de cours très pédagogiques :
  + [cours paris diderot](https://python.sdv.univ-paris-diderot.fr/)
  + [openclassroom](https://openclassrooms.com/fr/courses/) --> aller plus loin
  + [w3schools](https://www.w3schools.com/python/) --> syntaxe


---

# Où et comment écrire et exécuter du code python ? 

* Dans un interpréteur python
* Dans un notebook 
* Dans des fichiers sources (parfois appelés scripts)

---
# L'interpréteur python

L'interpréteur Python est le logiciel qui va transformer votre code source en suite d'instructions binaires et l'exécuter sur votre système. Une fois installé sur votre système, nous pouvons interagir avec lui en ligne de commande, dans un terminal la plupart du temps. 

Le triple chevron `>>>` est l'invite de commande (prompt en anglais) de l'interpréteur Python.

Ici, Python attend une commande que vous devez saisir au clavier : 

```python

>>> print("Hello world!")
Hello world!
>>>

```

---
# Les notebooks jupyter

On a vu ce qu'étaient les notebooks à la partie précédente. Dans les notebooks, les cellules *de code* peuvent être exécutées, c'est à dire que le code contenu dans ces cellules sera *envoyé* à un interpréteur. 

**Dans l'interface jupyterlite, vous pouvez créer/ouvrir/modifier des notebooks, mais aussi écrire des fichiers textes, des scripts, etc. Vous avez également accès à un interpréteur python.**


---
# Ecriture d'un programme

+ On peut rassembler toutes les instructions que l'on veut exécuter dans un fichier qui a l'extension **.py** : e.g. `programme_1.py`
+ L'interpréteur exécute alors les instructions contenues dans ce fichier
+ Les commentaires vont commencer par le caractère `#` : ces lignes ne seront pas exécutées
+ L'exécution se fait dans un terminal `python programme_1.py`

```
# Ce programme a pour but de calculer des moyennes à partir de données brutes
Instructions
# Lecture du fichier de données
Instructions
# Lecture du fichier de données de poids pour le calcul des moyennes
Instructions
```


---
Pour la suite du cours, nous vous proposons de revenir sur l'interface de [jupyterlite](https://bouttiep.gricad-pages.univ-grenoble-alpes.fr/jupyterlite/lab/)

---
