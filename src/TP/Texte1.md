Si c’est aimer, Madame, et de jour et de nuit
Rêver, songer, penser le moyen de vous plaire,
Oublier toute chose, et ne vouloir rien faire
Qu’adorer et servir la beauté qui me nuit ;
Si c’est aimer de suivre un bonheur qui me fuit,
De me perdre moi-même et d’être solitaire,
Souffrir beaucoup de mal, beaucoup craindre et me taire,
Pleurer, crier merci, et m’en voir éconduit ;
Si c’est aimer de vivre en vous plus qu’en moi-même,
Cacher d’un front joyeux une langueur extrême,
Sentir au fond de l’âme un combat inégal,
Chaud, froid, comme la fièvre amoureuse me traite,
Honteux, parlant à vous, de confesser mon mal ;
Si c’est cela aimer, furieux je vous aime.
Je vous aime, et sais bien que mon mal est fatal,
Le cœur le dit assez, mais la langue est muette.

Pierre de Ronsard
Sonnets pour Hélène, 
