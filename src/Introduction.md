---
marp: true
theme: gricad
author: C. Acary-Robet ; P-A Bouttier
paginate: true
footer: "Introduction à la programmation - CED2022"

---
# Objectifs de ce cours
# Plan détaillé

---
# Objectifs

+ Avoir des notions de programmation informatique
  + Vocabulaire
  + Mettre en place et utiliser les outils nécessaires
+ Etre capable de définir une démarche pour répondre à un problème posé
  + Définition du problème
  + Algorithme
+ Connaître les différents éléments du langage pour la mise en œuvre concrète

---
# Plan détaillé (1)

- Partie I : Introduction et contexte
  - Motivations pour la programmation
  - Historique
  - Fonctionnement d'un ordinateur
  - Les programmes/logiciels
  - Langage machine / compilation
  - Données

- Partie II : Concepts fondamentaux de la programmation
  - Algorithmie
  - Fonctionnalités diverses

---
# Plan détaillé (2)

- Partie III : Outils et bonnes pratiques
  - Bonnes pratiques de programmation
  - Reproductibilité
  - Outils d'aide

- Partie IV : Spécificité du langage python
  - Notion de variables
  - Ecriture des structures et des opérations
  - Entrées/sorties
  - Fonctions
  - Utilisation des librairies existantes

---
# Plan détaillé (3)

- Partie V : Traitement d'un problème spécifique avec Python
 - Appréhender et utiliser l'écosystème python (aperçu des modules existants)
 - Introduction au traitement statistique de données
 - Application à un cas simple : problème du traitement du langage (nombre d’occurrences d'un mot)
