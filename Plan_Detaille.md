# Plan détaillé

# Séance 1
## Partie I : Introduction et contexte (1h)

- Motivations pour la programmation
  - automatisation de tâche
  - traiter des gros volumes de données, gros volumes de calcul

- Historique
  - apparition des ordis + évolution des capacités de calcul (ordre de grandeur)
  - premiers algorithmes + évolution

- Fonctionnement d'un ordinateur
   - différents éléments + leur rôles (schéma de fonctionnement)
   - super-calculateur

- Les programmes/logiciels
  - Qu'est qu'un logiciel/programme ?
  - Les différents types de logiciels
  - Les interfaces utilisateurs (comment on interagit avec les logiciels)

- Langage machine / compilation
  - binaire
  - langage compilé / langage interprété
  - spécificité de python en tt que langage interprété

- Données
  - définition
  - types de données
  - format
  - stockage : fichier / dossier

## Partie II : Concepts fondamentaux de la programmation (1H30 48 slides)
- algorithmie
  - Définition, analogie avec une recette
  - Exemples

Pause : installation des outils

- notions de variables
  - Définition
  - Structures de données
  - Différents types de variables
- boucle
  - définition
  - Rôle, intérêt
  - exemples
- structure conditionnelle
- fonctions
- dépendances externes (librairies, sous-programmes, ...)
- différents types de programmation (paradigme : objet, fonctionnel )

# Séance 2

## Partie III : Outils et bonnes pratiques (1h 23 slides)
- éditeur de texte : visual code
- gestionnaire de version
- travail collaboratif
- commenter son code  : Exemples
- choix de noms de variables lisibles

# Séance 3 et 4
## Partie IV : Spécificité du langage python (60 slides)

- Notion de variables
- Ecriture des structures et des opérations
- Fonctions
- Utilisation des librairies existantes
- Entrées/sorties

# Séance 5
## Partie V : Traitement d'un problème spécifique avec Python

- Introduction au traitement statistique de données
- Appréhender et utiliser l'écosystème python (aperçu des modules existants)
- Application à un cas simple : problème du traitement du langage (nombre d’occurrences d'un mot)
